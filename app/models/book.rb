class Book < ActiveRecord::Base
  attr_accessible :author, :desc, :name
  has_many :comments
end
